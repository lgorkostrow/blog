<?php

namespace App\Tests\Infrastructure\Category\Service;

use App\Domain\Category\Model\Category;
use App\Infrastructure\Category\DTO\CategoryDTO;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryServiceTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();

        $container = self::$container;
        $categoryService = $container->get('App\Domain\Category\Service\CategoryServiceInterface');

        $categoryDTO = CategoryDTO::create(null, 'test create', 'test create');

        $result = $categoryService->create($categoryDTO);

        $this->assertInstanceOf(Category::class, $result);
        $this->assertEquals($categoryDTO->name, $result->getName());
    }

    public function testUpdate()
    {
        self::bootKernel();

        $container = self::$container;
        $categoryService = $container->get('App\Domain\Category\Service\CategoryServiceInterface');

        $category = $container->get('doctrine')->getRepository(Category::class)->findBy([], ['id' => 'DESC'], 1);
        $category = $category[0] ?? null;

        $categoryDTO = CategoryDTO::create(null, 'test update', 'test update');

        $result = $categoryService->update($category, $categoryDTO);

        $this->assertInstanceOf(Category::class, $result);
        $this->assertEquals($categoryDTO->name, $result->getName());
    }
}

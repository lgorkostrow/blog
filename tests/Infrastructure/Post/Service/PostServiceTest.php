<?php

namespace App\Tests\Infrastructure\Post\Service;

use App\Domain\Category\Model\Category;
use App\Domain\Post\Model\Post;
use App\Infrastructure\Post\DTO\PostDTO;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryServiceTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();

        $container = self::$container;
        $postService = $container->get('App\Domain\Post\Service\PostServiceInterface');

        $category = $container->get('doctrine')->getRepository(Category::class)->findBy([], ['id' => 'DESC'], 1);
        $category = $category[0] ?? null;

        $postDTO = PostDTO::create(null, 'test create', 'test create', $category);

        $result = $postService->create($postDTO);

        $this->assertInstanceOf(Post::class, $result);
        $this->assertEquals($postDTO->name, $result->getName());
    }

    public function testUpdate()
    {
        self::bootKernel();

        $container = self::$container;
        $postService = $container->get('App\Domain\Post\Service\PostServiceInterface');

        $post = $container->get('doctrine')->getRepository(Post::class)->findBy([], ['id' => 'DESC'], 1);
        $post = $post[0] ?? null;

        $postDTO = PostDTO::create(null, 'test update', 'test update');

        $result = $postService->update($post, $postDTO);

        $this->assertInstanceOf(Post::class, $result);
        $this->assertEquals($postDTO->name, $result->getName());
    }
}

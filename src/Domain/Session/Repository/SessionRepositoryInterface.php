<?php

namespace App\Domain\Session\Repository;

use App\Domain\Session\Model\Session;

interface SessionRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);
    public function findAll();
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
    public function getBrowserStats(): array;
    public function findOneBySessionId(string $sessionId);
    public function save(Session $session);
}

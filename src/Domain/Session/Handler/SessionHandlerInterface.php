<?php

namespace App\Domain\Session\Handler;

interface SessionHandlerInterface
{
    public function handle($sessionDTO);
}

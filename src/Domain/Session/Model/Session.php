<?php

namespace App\Domain\Session\Model;

class Session
{
    private $id;
    private $sessionId;
    private $browser;
    private $ip;

    public function __construct(string $sessionId, string $browser, string $ip)
    {
        $this->sessionId = $sessionId;
        $this->browser = $browser;
        $this->ip = $ip;
    }

    public static function create(string $sessionId, string $browser, string $ip)
    {
        return new self($sessionId, $browser, $ip);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    public function getBrowser(): string
    {
        return $this->browser;
    }

    public function getIp(): string
    {
        return $this->ip;
    }
}

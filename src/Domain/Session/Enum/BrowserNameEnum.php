<?php

namespace App\Domain\Session\Enum;

class BrowserNameEnum
{
    const MSIE = 'MSIE';
    const FIREFOX = 'FIREFOX';
    const CHROME = 'CHROME';
    const SAFARI = 'SAFARI';
    const OPERA = 'OPERA';

    const UNKNOWN = 'UNKNOWN';

    const VALID_BROWSERS = [
        self::MSIE,
        self::FIREFOX,
        self::CHROME,
        self::SAFARI,
        self::OPERA,
        self::UNKNOWN,
    ];
}

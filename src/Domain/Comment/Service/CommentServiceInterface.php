<?php

namespace App\Domain\Comment\Service;

use App\Domain\Category\Model\Category;
use App\Domain\Post\Model\Post;

interface CommentServiceInterface
{
    public function commentCategory(Category $category, $commentDTO);
    public function commentPost(Post $post, $commentDTO);
}

<?php

namespace App\Domain\Comment\Model;

use App\Domain\Category\Model\Category;
use App\Domain\Post\Model\Post;
use App\Infrastructure\Common\Model\TimestampableInterface;

class Comment implements TimestampableInterface
{
    private $id;
    private $author;
    private $content;
    private $createdAt;
    private $post;
    private $category;

    public function __construct(string $author, string $content)
    {
        $this->author = $author;
        $this->content = $content;
    }

    public static function create(string $author, string $content)
    {
        return new self($author, $content);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}

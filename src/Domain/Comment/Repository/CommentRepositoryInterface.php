<?php

namespace App\Domain\Comment\Repository;

interface CommentRepositoryInterface
{
    public function findByCategoryId(int $categoryId);
    public function findByPostId(int $postId);
}

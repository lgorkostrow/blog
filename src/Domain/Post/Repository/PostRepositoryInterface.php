<?php

namespace App\Domain\Post\Repository;

use App\Domain\Post\Model\Post;

interface PostRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);
    public function findAll();
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
    public function save(Post $post);
    public function remove(Post $post);
}

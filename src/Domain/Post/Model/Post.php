<?php

namespace App\Domain\Post\Model;

use App\Domain\Category\Model\Category;
use App\Domain\Comment\Model\Comment;
use Doctrine\Common\Collections\ArrayCollection;

class Post
{
    private $id;
    private $name;
    private $content;
    private $file;
    private $category;
    private $comments;

    public function __construct(string $name, string $content, Category $category, ?string $file)
    {
        $this->name = $name;
        $this->content = $content;
        $this->category = $category;
        $this->file = $file;
        $this->comments = new ArrayCollection();
    }

    public static function create(string $name, string $content, Category $category, ?string $file): self
    {
        return new self($name, $content, $category, $file);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function comment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $comment->setPost($this);
            $this->comments->add($comment);
        }

        return $this;
    }
}

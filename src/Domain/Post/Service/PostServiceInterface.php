<?php

namespace App\Domain\Post\Service;

use App\Domain\Comment\Model\Comment;
use App\Domain\Post\Model\Post;

interface PostServiceInterface
{
    public function create($postDTO);
    public function update(Post $post, $postDTO);
    public function comment(Post $post, Comment $comment);
}

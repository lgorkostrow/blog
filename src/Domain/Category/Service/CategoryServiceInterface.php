<?php

namespace App\Domain\Category\Service;

use App\Domain\Category\Model\Category;
use App\Domain\Comment\Model\Comment;

interface CategoryServiceInterface
{
    public function create($categoryDto);
    public function update(Category $category, $categoryDto);
    public function comment(Category $category, Comment $comment);
}

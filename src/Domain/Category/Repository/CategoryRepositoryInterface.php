<?php

namespace App\Domain\Category\Repository;

use App\Domain\Category\Model\Category;

interface CategoryRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);
    public function findAll();
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
    public function save(Category $category);
    public function remove(Category $category);
}

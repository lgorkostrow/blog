<?php

namespace App\Domain\Category\Model;

use App\Domain\Comment\Model\Comment;
use Doctrine\Common\Collections\ArrayCollection;

class Category
{
    private $id;
    private $name;
    private $description;
    private $posts;
    private $comments;

    public function __construct(string $name, string $description)
    {
        $this->name = $name;
        $this->description = $description;
        $this->posts = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public static function create(string $name, string $description): self
    {
        $category = new self($name, $description);

        return $category;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPosts()
    {
        return $this->posts;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function comment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $comment->setCategory($this);
            $this->comments->add($comment);
        }

        return $this;
    }
}

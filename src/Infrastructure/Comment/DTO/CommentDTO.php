<?php

namespace App\Infrastructure\Comment\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class CommentDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Assert\Regex("/^([A-Z,А-Я][a-я]*)\s+([A-Z,А-Я][a-я]*)$/m")
     */
    public $author;

    /**
     * @Assert\NotBlank
     */
    public $content;
}

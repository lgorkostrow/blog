<?php

namespace App\Infrastructure\Comment\Service;

use App\Domain\Category\Model\Category;
use App\Domain\Category\Service\CategoryServiceInterface;
use App\Domain\Comment\Model\Comment;
use App\Domain\Comment\Service\CommentServiceInterface;
use App\Domain\Post\Model\Post;
use App\Domain\Post\Service\PostServiceInterface;
use App\Infrastructure\Common\Mapper\DTOEntityMapper;

class CommentService implements CommentServiceInterface
{
    private $categoryService;
    private $postService;
    private $mapper;

    public function __construct(
        CategoryServiceInterface $categoryService,
        PostServiceInterface $postService,
        DTOEntityMapper $mapper
    )
    {
        $this->categoryService = $categoryService;
        $this->postService = $postService;
        $this->mapper = $mapper;
    }

    public function commentCategory(Category $category, $commentDTO): Comment
    {
        $comment = $this->mapper->mapDTOToNewEntity($commentDTO, Comment::class);
        $this->categoryService->comment($category, $comment);

        return $comment;
    }

    public function commentPost(Post $post, $commentDTO): Comment
    {
        $comment = $this->mapper->mapDTOToNewEntity($commentDTO, Comment::class);
        $this->postService->comment($post, $comment);

        return $comment;
    }
}

<?php

namespace App\Infrastructure\Comment\Repository;

use App\Domain\Comment\Model\Comment;
use App\Domain\Comment\Repository\CommentRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository implements CommentRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function findByCategoryId(int $categoryId)
    {
        return $this->findBy(['category' => $categoryId], ['id' => 'DESC']);
    }

    public function findByPostId(int $postId)
    {
        return $this->findBy(['post' => $postId], ['id' => 'DESC']  );
    }
}

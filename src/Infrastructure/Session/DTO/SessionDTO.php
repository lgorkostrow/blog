<?php

namespace App\Infrastructure\Session\DTO;

class SessionDTO
{
    public $sessionId;
    public $userAgent;
    public $ip;

    public function __construct(string $sessionId, string $userAgent, string $ip)
    {
        $this->sessionId = $sessionId;
        $this->userAgent = $userAgent;
        $this->ip = $ip;
    }

    public static function create(string $sessionId, string $userAgent, string $ip): self
    {
        return new self($sessionId, $userAgent, $ip);
    }
}

<?php

namespace App\Infrastructure\Session\EventListener;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class MarkUserEventListener
{
    public function onRequestEvent(RequestEvent $event)
    {
        $request = $event->getRequest();
        if ('json' === $request->getRequestFormat()) {
            return;
        }

        $session = $request->getSession();
        if (!$session instanceof SessionInterface) {
            return;
        }

        if (!$session->has('Session-ID')) {
            $session->set('Session-ID', $session->getId());
        }

        $request->headers->set('Session-ID', $session->getId());
    }
}

<?php

namespace App\Infrastructure\Session\EventListener;

use App\Domain\Session\Handler\SessionHandlerInterface;
use App\Infrastructure\Session\DTO\SessionDTO;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

class HandleSessionEventListener
{
    private $sessionHandler;

    public function __construct(SessionHandlerInterface $sessionHandler)
    {
        $this->sessionHandler = $sessionHandler;
    }

    public function onTerminateEvent(TerminateEvent $event)
    {
        $request = $event->getRequest();
        if (empty($request->headers->get('Session-ID'))) {
            return;
        }

        $sessionDTO = SessionDTO::create(
            $request->headers->get('Session-ID'),
            $request->headers->get('User-Agent'),
            $request->getClientIp()
        );

        $this->sessionHandler->handle($sessionDTO);
    }
}

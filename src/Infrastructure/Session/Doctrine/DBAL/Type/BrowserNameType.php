<?php

namespace App\Infrastructure\Session\Doctrine\DBAL\Type;

use App\Domain\Session\Enum\BrowserNameEnum;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class BrowserNameType extends Type
{
    const NAME = 'browser';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return sprintf(
            "ENUM('%s', '%s', '%s', '%s', '%s', '%s') COMMENT '(DC2Type: %s)'",
            BrowserNameEnum::MSIE,
            BrowserNameEnum::OPERA,
            BrowserNameEnum::SAFARI,
            BrowserNameEnum::CHROME,
            BrowserNameEnum::FIREFOX,
            BrowserNameEnum::UNKNOWN,
            self::NAME
        );
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!in_array($value, BrowserNameEnum::VALID_BROWSERS)) {
            throw new \InvalidArgumentException("Invalid browser");
        }
        return $value;
    }

    public function getName()
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

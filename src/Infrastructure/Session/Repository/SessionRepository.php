<?php

namespace App\Infrastructure\Session\Repository;

use App\Domain\Session\Repository\SessionRepositoryInterface;
use App\Domain\Session\Model\Session;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Session|null find($id, $lockMode = null, $lockVersion = null)
 * @method Session|null findOneBy(array $criteria, array $orderBy = null)
 * @method Session[]    findAll()
 * @method Session[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionRepository extends ServiceEntityRepository implements SessionRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Session::class);
    }

    public function findOneBySessionId(string $sessionId)
    {
        return $this->findOneBy(['sessionId' => $sessionId]);
    }

    public function getBrowserStats(): array
    {
        return $this->createQueryBuilder('s')
            ->select('count(s.browser) as count, s.browser')
            ->groupBy('s.browser')
            ->getQuery()
            ->getResult()
        ;
    }

    public function save(Session $session)
    {
        $this->_em->persist($session);
        $this->_em->flush($session);
    }
}


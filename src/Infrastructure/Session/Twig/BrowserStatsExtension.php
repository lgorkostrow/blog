<?php

namespace App\Infrastructure\Session\Twig;

use App\Domain\Session\Repository\SessionRepositoryInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BrowserStatsExtension extends AbstractExtension
{
    private $repository;

    public function __construct(SessionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getBrowserStats', [$this, 'getBrowserStats']),
        ];
    }

    public function getBrowserStats(): array
    {
        return $this->repository->getBrowserStats();
    }
}

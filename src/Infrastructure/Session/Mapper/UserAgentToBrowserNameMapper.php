<?php

namespace App\Infrastructure\Session\Mapper;

use App\Domain\Session\Enum\BrowserNameEnum;

class UserAgentToBrowserNameMapper
{
    private $list = [
        '/MSIE/i' => BrowserNameEnum::MSIE,
        '/Firefox/i' => BrowserNameEnum::FIREFOX,
        '/Chrome/i' => BrowserNameEnum::CHROME,
        '/Safari/i' => BrowserNameEnum::SAFARI,
        '/Opera/i' => BrowserNameEnum::OPERA,
    ];

    public function map(string $userAgent): string
    {
        foreach ($this->list as $key => $value) {
            if (!preg_match($key, $userAgent)) {
                continue;
            }

            return $value;
        }

        return BrowserNameEnum::UNKNOWN;
    }
}

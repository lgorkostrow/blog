<?php

namespace App\Infrastructure\Session\Handler;

use App\Domain\Session\Handler\SessionHandlerInterface;
use App\Domain\Session\Model\Session;
use App\Domain\Session\Repository\SessionRepositoryInterface;
use App\Infrastructure\Session\Mapper\UserAgentToBrowserNameMapper;

class SessionHandler implements SessionHandlerInterface
{
    private $repository;
    private $mapper;

    public function __construct(
        SessionRepositoryInterface $repository,
        UserAgentToBrowserNameMapper $mapper
    )
    {
        $this->repository = $repository;
        $this->mapper = $mapper;
    }

    public function handle($sessionDTO)
    {
        $session = $this->repository->findOneBySessionId($sessionDTO->sessionId);
        if (!empty($session)) {
            return;
        }

        $session = Session::create(
            $sessionDTO->sessionId,
            $this->mapper->map($sessionDTO->userAgent),
            $sessionDTO->ip
        );

        $this->repository->save($session);
    }
}

<?php

namespace App\Infrastructure\Post\Repository;

use App\Domain\Post\Model\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository implements PostRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function save(Post $post)
    {
        $this->_em->persist($post);
        $this->_em->flush($post);
    }

    public function remove(Post $post)
    {
        $this->_em->remove($post);
        $this->_em->flush($post);
    }
}

<?php

namespace App\Infrastructure\Post\Service;

use App\Domain\Comment\Model\Comment;
use App\Domain\Post\Model\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\Post\Service\PostServiceInterface;
use App\Infrastructure\Common\Mapper\DTOEntityMapper;

class PostService implements PostServiceInterface
{
    private $repository;
    private $mapper;

    public function __construct(PostRepositoryInterface $repository, DTOEntityMapper $mapper)
    {
        $this->repository = $repository;
        $this->mapper = $mapper;
    }

    public function create($postDTO): Post
    {
        $post = $this->mapper->mapDTOToNewEntity($postDTO, Post::class);
        $this->repository->save($post);

        return $post;
    }

    public function update(Post $post, $postDTO): Post
    {
        $post = $this->mapper->mapDTOToEntity($postDTO, $post);
        $this->repository->save($post);

        return $post;
    }

    public function comment(Post $post, Comment $comment): Post
    {
        $post->comment($comment);
        $this->repository->save($post);

        return $post;
    }
}

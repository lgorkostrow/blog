<?php

namespace App\Infrastructure\Post\DTO;

use App\Domain\Category\Model\Category;
use Symfony\Component\Validator\Constraints as Assert;

class PostDTO
{
    public $id;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public $name;

    /**
     * @Assert\NotBlank
     */
    public $content;

    /**
     * @Assert\NotBlank
     */
    public $category;

    /**
     * @Assert\File(maxSize = "2m")
     */
    public $file;

    public function __construct(
        int $id = null,
        string $name = null,
        string $content = null,
        Category $category = null,
        $file = null
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->content = $content;
        $this->category = $category;
        $this->file = $file;
    }

    public static function create(
        int $id = null,
        string $name = null,
        string $content = null,
        Category $category = null,
        $file = null
    ): self
    {
        return new self($id, $name, $content, $category, $file);
    }
}

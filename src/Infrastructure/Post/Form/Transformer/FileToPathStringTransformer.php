<?php

namespace App\Infrastructure\Post\Form\Transformer;

use App\Infrastructure\Common\Service\FileService;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileToPathStringTransformer implements DataTransformerInterface
{
    private $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function transform($value)
    {
        if (empty($value)) {
            return $value;
        }

        return $this->fileService->get($value);
    }

    public function reverseTransform($value)
    {
        if (!$value instanceof UploadedFile) {
            return $value;
        }

        return $this->fileService->save($value);
    }
}

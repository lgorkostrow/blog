<?php

namespace App\Infrastructure\Post\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class UpdatePostDTOType extends PostDTOType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->setMethod('PUT')
            ->add('save', SubmitType::class)
        ;
    }
}

<?php

namespace App\Infrastructure\Common\Model;

interface TimestampableInterface
{
    public function getCreatedAt(): ?\DateTimeInterface;
    public function setCreatedAt(\DateTimeInterface $createdAt);
}

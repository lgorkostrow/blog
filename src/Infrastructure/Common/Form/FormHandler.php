<?php

namespace App\Infrastructure\Common\Form;

use App\Infrastructure\Common\Formatter\ApiErrorFormatter;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class FormHandler
{
    private $formFactory;
    private $errorFormatter;

    public function __construct(FormFactoryInterface $formFactory, ApiErrorFormatter $errorFormatter)
    {
        $this->formFactory = $formFactory;
        $this->errorFormatter = $errorFormatter;
    }

    public function handleFromRequest(Request $request, string $type, $entity, array $options = []) {
        $form = $this->formFactory->create($type, $entity, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $form->getData();
        }

        return $form;
    }

    public function handleWithSubmit(
        array $data,
        // Form type classname
        string $type,
        // Initial data
        $entity,
        array $options = []
    ) {
        $form = $this->formFactory->create($type, $entity, $options);
        $form->submit($data);

        return $this->processSubmitted($form, $entity);
    }

    private function processSubmitted(Form $form, $entity)
    {
        if (!$form->isValid()) {
            return $this->errorFormatter->format($form);
        }

        if (!is_object($entity)) {
            $entity = $form->getData();
        }

        return $entity;
    }
}

<?php

namespace App\Infrastructure\Common\Service;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    private $basePath;
    private $relativePath;

    public function __construct(string $basePath, string $relativePath)
    {
        $this->basePath = rtrim($basePath, '/');
        $this->relativePath = trim($relativePath, '/');
    }

    public function get(string $pathToFile): File
    {
        return new File(
            sprintf('%s/%s', $this->basePath, $pathToFile)
        );
    }

    public function save(UploadedFile $file): string
    {
        $destination = $this->basePath.'/'.$this->relativePath;
        $file->move($destination, $file->getClientOriginalName());

        return $this->relativePath.'/'.$file->getClientOriginalName();
    }
}

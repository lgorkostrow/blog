<?php

namespace App\Infrastructure\Common\Mapper;

class DTOEntityMapper
{
    private const CREATE_METHOD_NAME = 'create';

    public function mapDTOToNewEntity($dtoObject, string $entityClass)
    {
        $dtoReflectionClass = new \ReflectionClass($dtoObject);
        $entityReflectionClass = new \ReflectionClass($entityClass);

        if ($entityReflectionClass->hasMethod('create')) {
            return $this->mapByCreateMethod($dtoObject, $entityClass, $dtoReflectionClass, $entityReflectionClass);
        } else {
            return $this->mapByConstructMethod($dtoObject, $entityClass, $dtoReflectionClass, $entityReflectionClass);
        }
    }

    public function mapDTOToEntity($dtoObject, $entity)
    {
        $dtoReflectionClass = new \ReflectionClass($dtoObject);
        $entityReflectionClass = new \ReflectionClass($entity);

        foreach ($dtoReflectionClass->getProperties() as $property) {
            if ($entityReflectionClass->hasProperty($property->getName())) {
                $value = $property->getValue($dtoObject);
                if (empty($value)) {
                    continue;
                }

                $entityProperty = $entityReflectionClass->getProperty($property->getName());
                $entityProperty->setAccessible(true);
                $entityProperty->setValue($entity, $value);
            }
        }

        return $entity;
    }

    private function mapByConstructMethod(
        $dtoObject,
        $entityClass,
        \ReflectionClass $dtoReflectionClass,
        \ReflectionClass $entityReflectionClass
    )
    {
        $args = $this->getArguments($dtoObject, $dtoReflectionClass, $entityReflectionClass->getConstructor());

        return new $entityClass(...$args);
    }

    private function mapByCreateMethod(
        $dtoObject,
        $entityClass,
        \ReflectionClass $dtoReflectionClass,
        \ReflectionClass $entityReflectionClass
    )
    {
        $createMethod = $entityReflectionClass->getMethod(self::CREATE_METHOD_NAME);

        $args = $this->getArguments($dtoObject, $dtoReflectionClass, $createMethod);

        return call_user_func([$entityClass, self::CREATE_METHOD_NAME], ...$args);
    }

    private function getArguments($object, \ReflectionClass $objectReflectionClass, \ReflectionMethod $method)
    {
        $args = [];

        foreach ($method->getParameters() as $parameter) {
            if ($objectReflectionClass->hasProperty($parameter->getName())) {
                $args[$parameter->getPosition()] = $objectReflectionClass->getProperty($parameter->getName())->getValue($object);
            }
        }

        return $args;
    }
}

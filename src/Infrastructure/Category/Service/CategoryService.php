<?php

namespace App\Infrastructure\Category\Service;

use App\Domain\Category\Model\Category;
use App\Domain\Category\Repository\CategoryRepositoryInterface;
use App\Domain\Category\Service\CategoryServiceInterface;
use App\Domain\Comment\Model\Comment;
use App\Infrastructure\Common\Mapper\DTOEntityMapper;

class CategoryService implements CategoryServiceInterface
{
    private $repository;
    private $mapper;

    public function __construct(CategoryRepositoryInterface $repository, DTOEntityMapper $mapper)
    {
        $this->repository = $repository;
        $this->mapper = $mapper;
    }

    public function create($categoryDto): Category
    {
        $category = $this->mapper->mapDTOToNewEntity($categoryDto, Category::class);
        $this->repository->save($category);

        return $category;
    }

    public function update(Category $category, $categoryDto): Category
    {
        $category = $this->mapper->mapDTOToEntity($categoryDto, $category);
        $this->repository->save($category);

        return $category;
    }

    public function comment(Category $category, Comment $comment): Category
    {
        $category->comment($comment);
        $this->repository->save($category);

        return $category;
    }
}

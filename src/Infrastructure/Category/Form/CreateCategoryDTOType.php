<?php

namespace App\Infrastructure\Category\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateCategoryDTOType extends CategoryDTOType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->setMethod('POST')
            ->add('save', SubmitType::class)
        ;
    }
}

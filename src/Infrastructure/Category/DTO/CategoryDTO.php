<?php

namespace App\Infrastructure\Category\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class CategoryDTO
{
    public $id;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public $name;

    /**
     * @Assert\NotBlank
     */
    public $description;

    public function __construct(int $id = null, string $name = null, string $description = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public static function create(int $id = null, string $name = null, string $description = null): self
    {
        return new self($id, $name, $description);
    }
}

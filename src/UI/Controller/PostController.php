<?php

namespace App\UI\Controller;

use App\Domain\Post\Model\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\Post\Service\PostServiceInterface;
use App\Infrastructure\Common\Form\FormHandler;
use App\Infrastructure\Common\Mapper\DTOEntityMapper;
use App\Infrastructure\Post\DTO\PostDTO;
use App\Infrastructure\Post\Form\CreatePostDTOType;
use App\Infrastructure\Post\Form\UpdatePostDTOType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    private $postRepository;
    private $postService;
    private $formHandler;
    private $mapper;

    public function __construct(
        PostRepositoryInterface $postRepository,
        PostServiceInterface $postService,
        FormHandler $formHandler,
        DTOEntityMapper $mapper
    )
    {
        $this->postRepository = $postRepository;
        $this->postService = $postService;
        $this->formHandler = $formHandler;
        $this->mapper = $mapper;
    }

    /**
     * @Route("/post", methods={"GET"})
     */
    public function list()
    {
        $posts = $this->postRepository->findAll();

        return $this->render('blog/post/post-list.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/post/{post}", methods={"GET"}, requirements={"post"="\d+"})
     */
    public function show(Post $post)
    {
        return $this->render('blog/post/post-show.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/post/new", methods={"GET", "POST"})
     */
    public function new(Request $request)
    {
        $handledData = $this->formHandler->handleFromRequest($request, CreatePostDTOType::class, new PostDTO());
        if ($handledData instanceof PostDTO) {
            $this->postService->create($handledData);

            return $this->redirectToRoute('app_ui_post_list');
        }

        return $this->render('blog/post/post-edit.html.twig', [
            'form' => $handledData->createView(),
        ]);
    }

    /**
     * @Route("/post/{post}/edit", methods={"GET", "PUT"}, requirements={"psot"="\d+"})
     */
    public function edit(Request $request, Post $post)
    {
        $handledData = $this->formHandler->handleFromRequest(
            $request,
            UpdatePostDTOType::class,
            PostDTO::create($post->getId(), $post->getName(), $post->getContent(), $post->getCategory(), $post->getFile())
        );
        if ($handledData instanceof PostDTO) {
            $this->postService->update($post, $handledData);

            return $this->redirectToRoute('app_ui_post_list');
        }

        return $this->render('blog/post/post-edit.html.twig', [
            'form' => $handledData->createView(),
        ]);
    }

    /**
     * @Route("/post/{post}/remove", methods={"GET"}, requirements={"post"="\d+"})
     */
    public function remove(Post $post)
    {
        $this->postRepository->remove($post);

        return $this->redirectToRoute('app_ui_post_list');
    }
}

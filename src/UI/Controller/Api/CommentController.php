<?php

namespace App\UI\Controller\Api;

use App\Domain\Category\Model\Category;
use App\Domain\Comment\Repository\CommentRepositoryInterface;
use App\Domain\Comment\Service\CommentServiceInterface;
use App\Domain\Post\Model\Post;
use App\Infrastructure\Comment\DTO\CommentDTO;
use App\Infrastructure\Comment\Form\CommentDTOType;
use App\Infrastructure\Common\Form\FormHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends AbstractFOSRestController
{
    private $repository;
    private $commentService;
    private $formHandler;

    public function __construct(
        CommentRepositoryInterface $repository,
        CommentServiceInterface $commentService,
        FormHandler $formHandler
    )
    {
        $this->repository = $repository;
        $this->commentService = $commentService;
        $this->formHandler = $formHandler;
    }

    /**
     * @Rest\Get("/api/comment/category/{categoryId}")
     *
     * @Rest\View()
     */
    public function getCategoryCommentsAction($categoryId)
    {
        return $this->repository->findByCategoryId($categoryId);
    }

    /**
     * @Rest\Post("/api/comment/category/{category}")
     *
     * @Rest\View()
     */
    public function postCategoryCommentAction(Category $category, Request $request)
    {
        $handledData = $this->formHandler->handleWithSubmit($request->request->all(), CommentDTOType::class, new CommentDTO());
        if (!$handledData instanceof CommentDTO) {
            return $this->view($handledData, 422);
        }

        return $this->commentService->commentCategory($category, $handledData);
    }

    /**
     * @Rest\Get("/api/comment/post/{postId}")
     *
     * @Rest\View()
     */
    public function getPostCommentsAction($postId)
    {
        return $this->repository->findByPostId($postId);
    }

    /**
     * @Rest\Post("/api/comment/post/{post}")
     *
     * @Rest\View()
     */
    public function postPostCommentAction(Post $post, Request $request)
    {
        $handledData = $this->formHandler->handleWithSubmit($request->request->all(), CommentDTOType::class, new CommentDTO());
        if (!$handledData instanceof CommentDTO) {
            return $this->view($handledData, 422);
        }

        return $this->commentService->commentPost($post, $handledData);
    }
}

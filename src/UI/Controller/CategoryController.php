<?php

namespace App\UI\Controller;

use App\Domain\Category\Model\Category;
use App\Domain\Category\Repository\CategoryRepositoryInterface;
use App\Domain\Category\Service\CategoryServiceInterface;
use App\Infrastructure\Category\DTO\CategoryDTO;
use App\Infrastructure\Category\Form\CreateCategoryDTOType;
use App\Infrastructure\Category\Form\UpdateCategoryDTOType;
use App\Infrastructure\Common\Form\FormHandler;
use App\Infrastructure\Common\Mapper\DTOEntityMapper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    private $categoryRepository;
    private $categoryService;
    private $formHandler;
    private $mapper;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        CategoryServiceInterface $categoryService,
        FormHandler $formHandler,
        DTOEntityMapper $mapper
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryService = $categoryService;
        $this->formHandler = $formHandler;
        $this->mapper = $mapper;
    }

    /**
     * @Route("/category", methods={"GET"})
     */
    public function list()
    {
        $categories = $this->categoryRepository->findAll();

        return $this->render('blog/category/category-list.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/category/{category}", methods={"GET"}, requirements={"category"="\d+"})
     */
    public function show(Category $category)
    {
        return $this->render('blog/category/category-show.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/category/new", methods={"GET", "POST"})
     */
    public function new(Request $request)
    {
        $handledData = $this->formHandler->handleFromRequest($request, CreateCategoryDTOType::class, new CategoryDTO());
        if ($handledData instanceof CategoryDTO) {
            $this->categoryService->create($handledData);

            return $this->redirectToRoute('app_ui_category_list');
        }

        return $this->render('blog/category/category-edit.html.twig', [
            'form' => $handledData->createView(),
        ]);
    }

    /**
     * @Route("/category/{category}/edit", methods={"GET", "PUT"}, requirements={"category"="\d+"})
     */
    public function edit(Request $request, Category $category)
    {
        $handledData = $this->formHandler->handleFromRequest(
            $request,
            UpdateCategoryDTOType::class,
            CategoryDTO::create($category->getId(), $category->getName(), $category->getDescription())
        );
        if ($handledData instanceof CategoryDTO) {
            $this->categoryService->update($category, $handledData);

            return $this->redirectToRoute('app_ui_category_list');
        }

        return $this->render('blog/category/category-edit.html.twig', [
            'form' => $handledData->createView(),
        ]);
    }

    /**
     * @Route("/category/{category}/remove", methods={"GET"}, requirements={"category"="\d+"})
     */
    public function remove(Category $category)
    {
        $this->categoryRepository->remove($category);

        return $this->redirectToRoute('app_ui_category_list');
    }
}
